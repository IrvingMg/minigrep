# Minigrep

Simple version of of the classic command line tool `grep` (globally search a regular expression and print).

## Usage

* Case-sensitive searching:

```bash
cargo run <string> <filename> < output.txt
```

* Case-insensitive searching:

```bash
set CASE_INSENSITIVE=1
cargo run <string> <filename> < output.txt
```

## Links

* [Rust tutorial](https://doc.rust-lang.org/book/ch12-00-an-io-project.html).
